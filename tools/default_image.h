/*
 * (C) Copyright 2009
 * Marvell Semiconductor <www.marvell.com>
 * Written-by: Prafulla Wadaskar <prafulla@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#ifndef _DEFAULT_IMAGE_H_
#define _DEFAULT_IMAGE_H_

void *fitimage_get_tparams (void);
void *image_get_tparams (void);
int image_check_params (struct mkimage_params *params);
int image_verify_header (char *ptr, int image_size,
			struct mkimage_params *params);
void image_print_header (char *ptr);
void image_set_header (char *ptr, struct stat *sbuf, int ifd,
			struct mkimage_params *params);
void fit_handle_file (struct mkimage_params *params);

#endif /* _DEFAULT_IMAGE_H_ */
